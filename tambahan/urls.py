from django.urls import path

from . import views

app_name = 'tambahan'

urlpatterns = [
    path('', views.index, name='stori'),
    path('gallery/', views.gallery, name='stori2')
]